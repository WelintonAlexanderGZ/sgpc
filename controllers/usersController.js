var conexion = require("../config/conexion");
var user = require("../model/user");
const bcryptjs = require("bcryptjs");

module.exports = {
    perfil: function (req, res) {
        res.render("users/perfil", {user:req.user});      
    },
    actualizarperfil: function (req, res) {
        user.obtenerdatos(conexion, req.user.id, function (err, datos) {
        sms = '';
        res.render("users/actualizarperfil", { user:req.user});     
        });             
    },
    verificarcon: async (req, res)=>{
        console.log(req.body.password); 
        console.log(req.body.id);
        user.obtenerpassword2(conexion, req.body.id,async(err, datos)=> {
        console.log(datos);
        if (datos.length == 0 || ! (await bcryptjs.compare(req.body.password, datos[0].password)))
        {
            s = 'Incorrecto';
            
        }else{
            s = 'Correcto';
        }
        res.render('users/actualizarperfil', {user:req.user, sms:s });       
        });
    },  
};