const jwt = require("jsonwebtoken");
const bcryptjs = require("bcryptjs");
const conexion = require("../config/conexion");
const { promisify } = require("util");
var auth = require("../model/auth");
const con = require("../config/conexion");


module.exports = {
  index: function (req, res) {
    auth.obtenerini(conexion, function (err, datos) {
      res.render("index", { libros: datos, user:req.user});    
    });
  },

  about: function (req, res) {
    res.render("about", {user:req.user});
  },
  
  register: function (req, res) {
    user = false
    res.render("register");
  },
  login: function (req, res) {
    user = false
    res.render("login", {alert: false});
  },
  guardar: async (req, res) => {
    try {
      const datos = req.body;
      let passHash = await bcryptjs.hash(datos.password, 8); 
      auth.insertar(conexion, datos, passHash, function (err) {
        res.redirect("/login");
      });  
    } catch (err) {
      console.log(err);
    }
  },

  ingresar: async (req, res) => {
    try {
      const datos = req.body;
      if (!datos.correo || !datos.password) {
        res.render('login',{
          alert:true,
          alertTitle: "Advertencia",
          alertMessage: "Ingrese un usuario y password",
          alertIcon:'info',
          showConfirmButton: true,
          timer: false,
          ruta: 'login'
      })
      }else{
        auth.logear(conexion, datos, async (err, results) =>{
          if (results.length == 0 || ! (await bcryptjs.compare(datos.password, results[0].password)))
          {
            res.render('login',{
              alert:true,
              alertTitle: "Error",
              alertMessage: "Usuario y/o Password incorrectas",
              alertIcon:'error',
              showConfirmButton: true,
              timer: false,
              ruta: 'login'
            })
          }else{
            //inicio correcto
            const id = results[0].id;
            const token = jwt.sign({id:id}, process.env.JWT_SECRETO, {
              expiresIn: process.env.JWT_TIEMPO_EXPIRA 
            })
            console.log("TOKEN: "+token+" para el USUARIO : "+ datos.password)

            const cookiesOptions = {
              expires: new Date(Date.now()+process.env.JWT_COOKIE_EXPIRES * 24 * 60 * 60 * 1000),
              httpOnly: true
            }
            res.cookie('jwt', token, cookiesOptions);
            res.render('login', {
              alert: true,
              alertTitle: "Conexión exitosa",
              alertMessage: "¡LOGIN CORRECTO!",
              alertIcon:'success',
              showConfirmButton: false,
              timer: 800,
              ruta: ''
            })
          }
        });
      }
    } catch (err) {
      console.log(err);
    }
  },

  consulta: async (req, res, next) => {
    if (req.cookies.jwt) {
      try {
        const decodificada = await promisify(jwt.verify)(req.cookies.jwt, process.env.JWT_SECRETO)
        auth.consultajwt(conexion, decodificada, (err, results) =>{
          if (!results) {return next()} {
            req.user = results[0]
            return next()
          }
        })
      } catch (err) {
        console.log(err)
        return next();
      }
    }else{
      req.user = false;
      return next();
    }
  },

  NoisAuthenticated: async (req, res, next) => {
    if (req.cookies.jwt) {
      try {
        const decodificada = await promisify(jwt.verify)(req.cookies.jwt, process.env.JWT_SECRETO)
        auth.consultajwt(conexion, decodificada, (err, results) =>{
          if (!results) {return next()} {
            req.user = results[0]
            return next()
          }
        })
      } catch (err) {
        console.log(err)
        return next();
      }
    }else{
      req.user = false;
      res.redirect ('/login');
    }
  },

  isAuthenticated: async (req, res, next) => {
    if (!req.cookies.jwt) {
      try {
        const decodificada = await promisify(jwt.verify)(req.cookies.jwt, process.env.JWT_SECRETO)
        auth.consultajwt(conexion, decodificada, (err, results) =>{
          if (!results) {return next()} {
            req.user = results[0]
            return next()
          }
        })
      } catch (err) {
        console.log(err)
        return next();
      }
    }else{
      req.user = false;
      res.redirect ('/');
    }
  },

  logout: (req, res)=>{
    res.clearCookie('jwt');
    console.log('cookie limpiada');
    return res.redirect('/');
  }
};


