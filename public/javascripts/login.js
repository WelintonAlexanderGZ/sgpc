const formulario = document.getElementById('formLogin');
const inputs = document.querySelectorAll('#formLogin input');

const expresiones = {
    epassword: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, // 8 caracters, una letra mayuscula y minuscula.
    ecorreo: /\S+@\S+\.\S+/ // formato valido de correo.
};

const validForm = (e) => {
    switch (e.target.name) {
       
        case "correo":
            if(expresiones.ecorreo.test(e.target.value)){
                document.getElementById("smserr1").innerHTML = "";
                a = false;
            } else{
                document.getElementById("smserr1").innerHTML = "Formato de correo no valido";
                a = true;
            }
        break;
        case "password":
            if(expresiones.epassword.test(e.target.value)){
                document.getElementById("smserr2").innerHTML = "";
                b = false;
            } else{
                document.getElementById("smserr2").innerHTML = "Mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula y un número.";
                b = true;
            }
        break;
    }

    if (a == false && b == false ) {
        btnIn.disabled =false;
     }else{
        btnIn.disabled = true;      
     }      
}

inputs.forEach((input) => {
    input.addEventListener('keyup', validForm);
    input.addEventListener('blur', validForm);
});