const formulario = document.getElementById('formRegister');
const inputs = document.querySelectorAll('#formRegister input');

const expresiones = {
    enombre_apellido: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, //Texto de al menos 50 digitos.
    ecorreo: /\S+@\S+\.\S+/, // formato valido de correo.
    entelefono: /^\d{10}$/, // numérico de 10 dígitos.
    epassword: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/, // 8 caracters, una letra mayuscula y minuscula.   
};

const validForm = (e) => {

    switch (e.target.name) {

        case "nombre_apellido":
            if(expresiones.enombre_apellido.test(e.target.value)){
                document.getElementById("smserr1").innerHTML = "";
                a = false;
            } else{
                document.getElementById("smserr1").innerHTML = "No ingrese datos numericos / Campo en blanco";
                a = true;
            }
        break;
        case "correo":
            if(expresiones.ecorreo.test(e.target.value)){
                document.getElementById("smserr2").innerHTML = "";
                b = false;
            } else{
                document.getElementById("smserr2").innerHTML = "Formato de correo no valido";
                b = true;
            }
        break;
        case "ntelefono":
            if(expresiones.entelefono.test(e.target.value)){
                document.getElementById("smserr3").innerHTML = "";
                c = false;
            } else{
                document.getElementById("smserr3").innerHTML = "Su telefono debe tener 10 caracteres numericos";
                c = true;
            }
        break;
        case "password":
            if(expresiones.epassword.test(e.target.value)){
                document.getElementById("smserr4").innerHTML = "";
                d = false;
            } else{
                document.getElementById("smserr4").innerHTML = "Mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula y un número";
                d = true;
            }
        break;
    }
    var pass1 = $('[name=password]');
	var pass2 = $('[name=cpassword]');
    var valor1 = pass1.val();
    var valor2 = pass2.val();
    if (valor1 == valor2 ) {
        document.getElementById("smserr5").innerHTML = "";
        f = false;
    }else{
        document.getElementById("smserr5").innerHTML = "Las contraseñas no coinciden";
        f = true;
    }

    if (a == false && b == false && c == false && d == false && f == false) {
        btnIn.disabled =false;
     }else{
        btnIn.disabled = true;      
     }      
}

inputs.forEach((input) => {
    input.addEventListener('keyup', validForm);
    input.addEventListener('blur', validForm);
});

