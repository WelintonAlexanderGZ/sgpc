function initMap() {
    // Latitude and Longitude
    var myLatLng = {lat: -0.954524, lng: -80.746116};

    var map = new google.maps.Map(document.getElementById('google-maps'), {
        zoom: 17,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Universidad Laica Eloy Alfaro de Manabi' // Title Location
    });
}