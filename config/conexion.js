var mysql = require("mysql");
var con = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
});
con.connect(
    (err)=>{
        if (!err) {
            console.log('Conexion establecida con MySQL');
            console.log();
        }else{
            console.log('Error de conexion'+err);
        }
    }
);

module.exports=con;