var express = require("express");
const usersController = require("../controllers/usersController");
const authController = require("../controllers/authController");

var router = express.Router();

router.get("/perfil",  authController.NoisAuthenticated, usersController.perfil);
router.get("/actualizarperfil",  authController.NoisAuthenticated, usersController.actualizarperfil);
router.post("/actualizarperfil", authController.NoisAuthenticated, usersController.verificarcon);
module.exports = router;
