var express = require("express");
const authController = require("../controllers/authController");
const librosController = require("../controllers/librosController.js");
var router = express.Router();

/* GET home page. */

router.get("/", authController.consulta,authController.index);
router.get("/register", authController.isAuthenticated,authController.register);
router.get("/login", authController.isAuthenticated,authController.login);
router.get("/about",authController.consulta, authController.about);

router.post("/register", authController.guardar);
router.post("/login", authController.ingresar);
router.get("/logout", authController.logout);
module.exports = router;
